import { ReactDataContext } from "@themost/react";

export class GradesService {
  /**
   * @param {ReactDataContext} context
   */
  constructor(context) {
    this.context = context;
  }

  getGradeInfo() {
    return this.context.model('students/me/courses')
      .asQueryable()
      .expand('course($expand=locale)',
        'courseType($expand=locale)',
        'gradeExam($expand=instructors($expand=instructor($select=InstructorSummary)))')
      .orderByDescending('semester')
      .thenByDescending('gradeYear')
      .take(-1)
      .getItems();
  };

  getGradeInfoSearch(searchText) {
    return this.context.model('students/me/courses')
      .asQueryable()
      .expand('course($expand=locale)',
        'courseType($expand=locale)',
        'gradeExam($expand=instructors($expand=instructor($select=InstructorSummary)))')
      .where('course/name').contains(searchText)
      .or('course/displayCode').contains(searchText)
      .orderByDescending('semester')
      .thenByDescending('gradeYear')
      .take(-1)
      .getItems();
  }

  getThesesInfo() {
    return this.context.model('students/me/theses')
      .asQueryable()
      .expand('results($orderby=index;$expand=instructor($select=InstructorSummary)),thesis($expand=instructor($select=InstructorSummary),locale)')
      .take(-1)
      .getItems();
  }

  getLastExamPeriod() {
    return this.context.model('students/me/grades')
      .select('courseExam/year as gradeYear', 'courseExam/examPeriod as examPeriod')
      .orderByDescending('courseExam/year')
      .thenByDescending('courseExam/examPeriod')
      .take(-1)
      .getItem();
  }

  getCourseTeachers() {
    return this.context.model('students/me/classes')
      .asQueryable()
      .select('course/id as id, courseClass')
      .expand('courseClass($expand=instructors($expand=instructor($select=InstructorSummary)))')
      .take(-1)
      .getItems();
  }

  getRecentGrades() {
    // get last examination period
    return this.getLastExamPeriod().then((lastExaminationPeriod) => {
      // get last grade year
      const lastGradeYear = lastExaminationPeriod && lastExaminationPeriod.gradeYear;
      // get last exam period
      const lastExamPeriod = lastExaminationPeriod && lastExaminationPeriod.examPeriod;
      if (typeof lastGradeYear === 'undefined' || typeof lastExamPeriod === 'undefined') {
        // return empty array
        return Promise.resolve([]);
      }
      // get courses (expand course attributes, exam instructors)
      return this.context.model('students/me/grades')
        .where('courseExam/year').equal(lastGradeYear)
        .and('courseExam/examPeriod').equal(lastExamPeriod)
        .expand('status', 'course($expand=gradeScale,locale)',
          'courseClass($expand=instructors($expand=instructor($select=InstructorSummary)))',
          'courseExam($expand=examPeriod,year)')
        .take(-1)
        .getItems();
    });
  }
}
