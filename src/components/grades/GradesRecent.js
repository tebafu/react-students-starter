import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { ApplicationContext } from "../../ApplicationContext";
import { Alert } from "react-bootstrap";
import { GradesService } from "../../services/GradesService";
import { RecentGradesTable } from "./RecentGradesTable";
import { RecentGradesStats } from "./RecentGradesStats";
import { Loading } from "../Loading";

export function GradesRecent() {
  const { t } = useTranslation();
  const [grades, setGrades] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  /**
   * @type {{context:import('@themost/react').ReactDataContext}}
   */
  const { context } = useContext(ApplicationContext);
  const gradesService = new GradesService(context);

  useEffect(() => {
    gradesService.getRecentGrades().then(res => {
      setGrades(res);
      setError(null);
    }).catch(err => {
      setError(err);
      setGrades([]);
    }).finally(() => {
      setLoading(false);
    });
  }, []);

  if (loading) return <Loading />;
  return (
    <>
      <h1 className="mb-4">
        {t('Recent Grades')}
      </h1>
      {loading && (
        <div>Loading...</div>
      )}
      {error && (
        <Alert variant="danger">
          {error.message}
        </Alert>
      )}
      {!error && !loading && (
        <>
          {grades[0] && (
            <h2 className="py-4">
              <span>{t('Exam Period')}</span>:{" "}
              {grades[0].courseExam.examPeriod.name} {grades[0].courseExam.year.name}
            </h2>
          )}
          <div className="d-block d-md-flex flex-row w-100">
            <RecentGradesTable grades={grades} />
            <div className="ml-md-4 mr-md-1">
              <RecentGradesStats grades={grades} />
            </div>
          </div>
        </>
      )}
    </>
  );
}
