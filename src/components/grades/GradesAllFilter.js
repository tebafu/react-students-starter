import { useTranslation } from 'react-i18next';
import './grades.filter.css';
import { useState } from 'react';
import { InputGroup, Spinner } from 'react-bootstrap';

export function GradesAllFilter({ orderBy, setOrderBy, doSearch, searchLoading }) {
  const { t } = useTranslation();
  const [searchText, setSearchText] = useState('');

  return (
    <div className="grade_filter-area d-sm-flex flex-wrap rounded d-none d-lg-block mt-3 mb-3">
      <form
        onSubmit={(e) => e.preventDefault()}
        noValidate=""
        className="form-inline p-3 col ng-untouched ng-pristine ng-valid"
      >
        <div className="form-group col-4 col-sm-5 flex-nowrap">
          <label htmlFor="searchText" className="pr-4">
            {t('Order By')}
          </label>
          <select
            className="form-control flex-fill"
            onChange={(e) => setOrderBy(e.target.value)}
            defaultValue={orderBy}
          >
            <option value="semester">
              {t('Semester')}
            </option>
            <option value="courseType">
              {t('Course Type')}
            </option>
          </select>
        </div>
        <div className="form-group col-8 col-sm-7 flex-nowrap">
          <label htmlFor="searchText" className="pr-4">
            {t('Filter')}
          </label>
          <InputGroup aria-label="Search" className='w-100'>
            <Spinner animation="grow" role="status" size="sm" className={searchLoading ? 'my-auto mr-2' : 'd-none'} />
            <input
              spellCheck="false"
              type="text"
              id="searchText"
              name="searchText"
              className="form-control col text-left"
              placeholder={t('Subject name or code')}
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  doSearch(searchText);
                }
              }}
            />
          </InputGroup>

        </div>
      </form>
    </div>
  );
}
