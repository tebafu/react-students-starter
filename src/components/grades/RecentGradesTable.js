import { faFlagCheckered, faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card } from "react-bootstrap";
import { useTranslation } from "react-i18next";

export function RecentGradesTable({ grades }) {
  const { t } = useTranslation();

  return (
    <div className="d-flex flex-column w-100">
      {grades.map((grade) => (
        <Card key={grade.id} className='mb-3' >
          <Card.Body>
            <div className="d-flex align-items-center">
              <div>
                <Card.Title>
                  {grade.courseClass.title}
                  <h6 className="font-weight-normal">
                    {grade.courseType?.name}
                  </h6>
                </Card.Title>
                <Card.Text>
                  <span className="text-muted mr-2">
                    <FontAwesomeIcon icon={faFlagCheckered} />{" "}
                    {grade.course.displayCode}
                  </span>
                  <span className="text-muted">
                    <FontAwesomeIcon icon={faUserSecret} />{" "}
                    {
                      grade.courseClass.instructors.map((x) => (
                        x.instructor.familyName + ' ' + x.instructor.givenName
                      )).join(', ')
                    }
                  </span>
                </Card.Text>
              </div>
              <div className='ml-auto d-flex align-items-center'>
                <a href='#' className='btn btn-outline-secondary rounded-pill btn-more btn-block my-4 disabled'>
                  <span className='px-4'>
                    {t('Statistics')}
                  </span>
                </a>
                <span className="font-2xl col-1 text-nowrap text-right font-weight-bold d-sm-block float-right">
                  {grade.isPassed ? <span className="text-success">{grade.formattedGrade}</span> : <span className="text-danger">{grade.formattedGrade}</span>}
                </span>
              </div>
            </div>
          </Card.Body>
        </Card >
      ))}
    </div>
  );
}
