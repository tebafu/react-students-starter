import clsx from "clsx";
import { useTranslation } from "react-i18next";

export function GradesAllToolbar({ passedOnly, setPassedOnly }) {
  const { t } = useTranslation();

  return (
    <div role="toolbar" className="btn-toolbar my-3 d-sm-flex justify-content-end">
      <div role="group" className="btn-group">
        <button
          type="button"
          className={clsx("btn", !passedOnly ? "btn-primary" : "btn-gray-100 text-gray-500")}
          onClick={(e) => setPassedOnly(false)}
        >
          {t('All Courses')}
        </button>
        <button
          type="button"
          className={clsx("btn", passedOnly ? "btn-primary" : "btn-gray-100 text-gray-500")}
          onClick={(e) => setPassedOnly(true)}
        >
          {t('Only Passed')}
        </button>
      </div>
    </div>
  );
}
